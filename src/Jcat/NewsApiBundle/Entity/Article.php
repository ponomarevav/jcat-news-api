<?php

namespace Jcat\NewsApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Jcat\NewsApiBundle\Model\ArticleInterface;

/**
 * Article
 *
 * @ORM\Table(name="article", indexes={@Index(name="title_idx", columns={"title"}, flags={"fulltext"}),
 * @Index(name="short_descr_idx", columns={"short_descr"}, flags={"fulltext"}),
 * @Index(name="descr_idx", columns={"descr"}, flags={"fulltext"}),
 * @Index(name="source_idx", columns={"source"}),
 * @Index(name="created_idx", columns={"created"}),
 * @Index(name="published_idx", columns={"published"})})
 * @ORM\Entity(repositoryClass="Jcat\NewsApiBundle\Repository\ArticleRepository")
 */
class Article implements ArticleInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="short_descr", type="string", length=500)
     */
    private $shortDescr;

    /**
     * @var string
     *
     * @ORM\Column(name="descr", type="text")
     */
    private $descr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", options={"default": 0})
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="mtime", type="datetime", columnDefinition="DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL")
     */
    private $mtime;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="string", length=255)
     */
    private $source;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=500)
     */
    private $url;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="published", type="datetime")
     */
    private $published;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Article
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set shortDescr
     *
     * @param string $shortDescr
     *
     * @return Article
     */
    public function setShortDescr($shortDescr)
    {
        $this->shortDescr = $shortDescr;

        return $this;
    }

    /**
     * Get shortDescr
     *
     * @return string
     */
    public function getShortDescr()
    {
        return $this->shortDescr;
    }

    /**
     * Set descr
     *
     * @param string $descr
     *
     * @return Article
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;

        return $this;
    }

    /**
     * Get descr
     *
     * @return string
     */
    public function getDescr()
    {
        return $this->descr;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Article
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set mtime
     *
     * @param \DateTime $mtime
     *
     * @return Article
     */
    public function setMtime($mtime)
    {
        $this->mtime = $mtime;

        return $this;
    }

    /**
     * Get mtime
     *
     * @return \DateTime
     */
    public function getMtime()
    {
        return $this->mtime;
    }

    /**
     * Set source
     *
     * @param string $source
     *
     * @return Article
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Article
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set published
     *
     * @param \DateTime $published
     *
     * @return Article
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return \DateTime
     */
    public function getPublished()
    {
        return $this->published;
    }
}

