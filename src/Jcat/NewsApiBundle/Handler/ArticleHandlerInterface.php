<?php

namespace Jcat\NewsApiBundle\Handler;

use Jcat\NewsApiBundle\Model\ArticleInterface;

interface ArticleHandlerInterface
{
    /**
     * Получить новость по идентификатору
     *
     * @param mixed $id
     *
     * @return ArticleInterface
     */
    public function get($id);

    /**
     * Получить список новостей
     *
     * @param int $page номер страницы
     * @param int $limit кол-во записей на странице
     * @param array|null $sorting параметры сортировки
     *
     * @return array
     */
    public function all($page = 1, $limit = 10, array $sorting = null);
}