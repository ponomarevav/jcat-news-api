<?php

namespace Jcat\NewsApiBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use Jcat\NewsApiBundle\Model\ArticleInterface;

class ArticleHandler implements ArticleHandlerInterface
{
    private $om;
    private $entityClass;
    private $repository;
    private $formFactory;

    public function __construct(ObjectManager $om, $entityClass, FormFactoryInterface $formFactory)
    {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
    }

    /**
     * Получить новость по id
     *
     * @param mixed $id
     *
     * @return ArticleInterface
     */
    public function get($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Получить список страниц
     *
     * @param int $page номер страницы
     * @param int $limit кол-во записей на странице
     * @param array|null $sorting параметры сортировки
     *
     * @return array
     */
    public function all($page = 1, $limit = 10, array $sorting = null)
    {
        return $this->repository->findBy(array(), $sorting, $limit, ($page - 1) * $limit);
    }
}