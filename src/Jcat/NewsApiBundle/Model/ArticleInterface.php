<?php

namespace Jcat\NewsApiBundle\Model;

Interface ArticleInterface
{
    /**
     * Set title
     *
     * @param string $title
     *
     * @return ArticleInterface
     */
    public function setTitle($title);

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle();

    /**
     * Set shortDescr
     *
     * @param string $shortDescr
     *
     * @return ArticleInterface
     */
    public function setShortDescr($shortDescr);

    /**
     * Get shortDescr
     *
     * @return string
     */
    public function getShortDescr();

    /**
     * Set descr
     *
     * @param string $descr
     *
     * @return ArticleInterface
     */
    public function setDescr($descr);

    /**
     * Get descr
     *
     * @return string
     */
    public function getDescr();

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return ArticleInterface
     */
    public function setCreated($created);

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated();

    /**
     * Set mtime
     *
     * @param \DateTime $mtime
     *
     * @return ArticleInterface
     */
    public function setMtime($mtime);

    /**
     * Get mtime
     *
     * @return \DateTime
     */
    public function getMtime();

    /**
     * Set source
     *
     * @param string $source
     *
     * @return ArticleInterface
     */
    public function setSource($source);

    /**
     * Get source
     *
     * @return string
     */
    public function getSource();

    /**
     * Set url
     *
     * @param string $url
     *
     * @return ArticleInterface
     */
    public function setUrl($url);

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl();

    /**
     * Set published
     *
     * @param \DateTime $published
     *
     * @return ArticleInterface
     */
    public function setPublished($published);

    /**
     * Get published
     *
     * @return \DateTime
     */
    public function getPublished();
}