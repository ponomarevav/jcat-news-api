<?php

namespace Jcat\NewsApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Jcat\NewsApiBundle\Model\ArticleInterface;

class ArticleController extends FOSRestController
{
    /**
     * List all pages.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Возвращет в случае успеха."
     *   }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", nullable=true, description="Number of page that need to be extracted.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="10", description="NUmber of items on page.")
     * @Annotations\QueryParam(name="sorting", requirements="\w+", nullable=true, description="Sorting field.")
     * @Annotations\QueryParam(name="direction", requirements="\w+", default="asc", description="Direction of sorting.")
     *
     *
     * @Annotations\View(
     *  template = "JcatNewsApiBundle:Article:getArticles.html.twig",
     *  templateVar="articles"
     * )
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getArticlesAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $page = $paramFetcher->get('page');

        $page = is_null($page) ? 1 : $page;
        $limit = $paramFetcher->get('limit');
        $sorting = $paramFetcher->get('sorting');
        $direction = strtolower($paramFetcher->get('direction'));

        if ($sorting) {
            if (!in_array($sorting, ['created', 'title'])) {
                throw new BadRequestHttpException('Указано не допустимое поле сортировки');
            }

            if (!in_array($direction, ['asc', 'desc'])) {
                throw new BadRequestHttpException('Указано не допустимое направление сортировки');
            }

            $sorting = [$sorting => $direction];
        }

        return $this->container->get('jcat_newsapi.article.handler')->all($page, $limit, $sorting);
    }

    /**
     * Get single Page.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Получить новость по указанному id",
     *   output = "Jcat\NewsApiBundle\Entity\Article",
     *   statusCodes = {
     *     200 = "Запрос успешно выполнен",
     *     404 = "Новость не найдена"
     *   }
     * )
     *
     * @Annotations\View(
     *     template = "JcatNewsApiBundle:Article:getArticle.html.twig",
     *     templateVar="article"
     * )
     *
     * @param int $id идентификатор новости
     *
     * @throws NotFoundHttpException когда не найдена
     *
     * @return array
     */
    public function getArticleAction($id)
    {
        $page = $this->fetchArticle($id);

        return $page;
    }

    /**
     * Извлечение страницы
     *
     * @param mixed $id
     *
     * @throws NotFoundHttpException
     *
     * @return ArticleInterface
     */
    protected function fetchArticle($id)
    {
        $article = $this->container->get('jcat_newsapi.article.handler')->get($id);

        if (!$article) {
            throw new NotFoundHttpException(sprintf('Новость \'%s\' не найдена.',$id));
        }

        return $article;
    }
}