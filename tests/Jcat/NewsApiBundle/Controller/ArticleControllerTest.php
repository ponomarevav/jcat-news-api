<?php

namespace Tests\Jcat\NewsApiBundle\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase as WebTestCase;
use Tests\Jcat\NewsApiBundle\Fixtures\Entity\ArticleData;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\HttpFoundation\Response;

class ArticleControllerTest extends WebTestCase
{
    /**
     * @var Client;
     */
    protected $client;

    /**
     * @var array
     */
    protected $auth = [];

    public function setUp()
    {
        $this->auth = array(
            'PHP_AUTH_USER' => 'jcat',
            'PHP_AUTH_PW'   => 'test',
        );

        $this->client = static::createClient(array(), $this->auth);
    }

    public function testJsonGetArticleAction()
    {
        $fixtures = array(ArticleData::class);
        $this->loadFixtures($fixtures);
        $articles = ArticleData::$articles;
        $article = array_pop($articles);

        $route =  $this->getUrl('api_1_get_article', array('id' => $article->getId(), '_format' => 'json'));

        $this->client->request('GET', $route, array('ACCEPT' => 'application/json'));
        $response = $this->client->getResponse();

        $this->assertJsonResponse($response, 200);
        $content = $response->getContent();

        $decoded = json_decode($content, true);
        $this->assertTrue(isset($decoded['id']));
    }

    public function testHeadRoute()
    {
        $fixtures = array(ArticleData::class);
        $this->loadFixtures($fixtures);
        $articles = ArticleData::$articles;
        $article = array_pop($articles);

        $this->client->request(
            'HEAD',  
            sprintf('/api/v1/articles/%d.json', $article->getId()),
            array('ACCEPT' => 'application/json')
        );
        $response = $this->client->getResponse();
        $this->assertJsonResponse($response, 200, false);
    }

    /**
     * @param Response $response
     * @param int $statusCode
     * @param bool $checkValidJson
     * @param string $contentType
     */
    protected function assertJsonResponse(
        $response, 
        $statusCode = 200, 
        $checkValidJson =  true, 
        $contentType = 'application/json'
    ) {
        $this->assertEquals(
            $statusCode, $response->getStatusCode(),
            $response->getContent()
        );
        $this->assertTrue(
            $response->headers->contains('Content-Type', $contentType),
            $response->headers
        );

        if ($checkValidJson) {
            $decode = json_decode($response->getContent());
            $this->assertTrue(($decode != null && $decode != false),
                'is response valid json: [' . $response->getContent() . ']'
            );
        }
    }
}
