<?php

namespace Tests\Jcat\NewsApiBundle\Handler;

use Jcat\NewsApiBundle\Handler\ArticleHandler;
use Jcat\NewsApiBundle\Entity\Article;
use PHPUnit_Framework_MockObject_MockObject;
use Symfony\Component\Form\FormFactoryInterface;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\Mapping\ClassMetadata;

class ArticleHandlerTest extends \PHPUnit_Framework_TestCase
{
    /** @var ArticleHandler */
    protected $articleHandler;

    /** @var PHPUnit_Framework_MockObject_MockObject */
    protected $om;

    /** @var PHPUnit_Framework_MockObject_MockObject */
    protected $repository;

    /** @var  FormFactoryInterface */
    protected $formFactory;

    public function setUp()
    {
        $class = $this->getMock(ClassMetadata::class);
        $this->om = $this->getMock(ObjectManager::class);
        $this->repository = $this->getMock(ObjectRepository::class);
        $this->formFactory = $this->getMock(FormFactoryInterface::class);

        $this->om->expects($this->any())
            ->method('getRepository')
            ->with($this->equalTo(Article::class))
            ->will($this->returnValue($this->repository));
        $this->om->expects($this->any())
            ->method('getClassMetadata')
            ->with($this->equalTo(Article::class))
            ->will($this->returnValue($class));
        $class->expects($this->any())
            ->method('getName')
            ->will($this->returnValue(Article::class));
    }

    public function testGet()
    {
        $id = 1;
        $article = $this->getArticle();
        $this->repository->expects($this->once())->method('find')
            ->with($this->equalTo($id))
            ->will($this->returnValue($article));

        $this->articleHandler = $this->createArticleHandler($this->om, Article::class,  $this->formFactory);
        $this->articleHandler->get($id);
    }

    public function testAll()
    {
        $page = 1;
        $limit = 2;
        $sorting = ['created' => 'desc'];

        $articles = $this->getArticles(2);
        $this->repository->expects($this->once())->method('findBy')
            ->with(array(), ['created' => 'desc'], $limit, ($page - 1) * $limit)
            ->will($this->returnValue($articles));

        $this->articleHandler = $this->createArticleHandler($this->om, Article::class,  $this->formFactory);

        $all = $this->articleHandler->all($page, $limit, $sorting);

        $this->assertEquals($articles, $all);
    }


    protected function createArticleHandler($objectManager, $entityClass, $formFactory)
    {
        return new ArticleHandler($objectManager, $entityClass, $formFactory);
    }

    protected function getArticle()
    {
        return new Article();
    }

    protected function getArticles($numItems = 5)
    {
        $articles = array();

        for($i = 0; $i < $numItems; $i++) {
            $articles[] = $this->getArticle();
        }

        return $articles;
    }
}
