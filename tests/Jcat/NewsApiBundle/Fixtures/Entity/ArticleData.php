<?php

namespace Tests\Jcat\NewsApiBundle\Fixtures\Entity;

use Jcat\NewsApiBundle\Entity\Article;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DateTime;

class ArticleData implements FixtureInterface
{
    static public $articles = array();

    public function load(ObjectManager $manager)
    {
        $time = new DateTime('2016-03-04 13:00:00');

        $article = new Article();
        $article->setTitle("Сгорел дом");
        $article->setSource('РБК');
        $article->setCreated($time);
        $modTime = clone $time;
        $article->setMtime($modTime->setTime(15, 30, 0));
        $article->setShortDescr("На окраине города подожгли дом");
        $article->setDescr("На окраине города подожгли дом, никого не нашли");
        $pubTime = clone $modTime;
        $article->setPublished($pubTime->setDate(2016, 3, 5));
        $article->setUrl('http://rbc.ru/article/1');
        $manager->persist($article);
        self::$articles[] = $article;

        $article2 = new Article();
        $article2->setTitle("НАТО расписалось в бессилии против угрозы России");
        $article2->setSource('Ведомости');
        $time2 = clone $time;
        $article2->setCreated($time2->setTime(18,45, 0));
        $article2->setMtime($modTime);
        $article2->setShortDescr("НАТО признало не возможность защитить союзников в Европе против военной угрозы России");
        $article2->setDescr("Эштон Картер просит у конгресса увеличить расходы на оборону на $100 млр.");
        $pubTime = clone $modTime;
        $article2->setPublished($pubTime->setDate(2016, 3, 5));
        $article2->setUrl('http://vedomosti.ru/article/2');
        $manager->persist($article2);
        self::$articles[] = $article2;

        $manager->flush();
    }
}
